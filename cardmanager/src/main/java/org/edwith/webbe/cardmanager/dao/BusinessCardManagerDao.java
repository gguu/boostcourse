package org.edwith.webbe.cardmanager.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.edwith.webbe.cardmanager.dto.BusinessCard;

public class BusinessCardManagerDao {
	private static String dburl = "jdbc:mysql://localhost:3306/connectdb?characterEncoding=UTF-8&serverTimezone=UTC";
	private static String dbUser = "connectuser";
	private static String dbpasswd = "rootroot";
	
    public List<BusinessCard> searchBusinessCard(String keyword){
    	List<BusinessCard> list = null;
    	
    	Connection conn = null;
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	
    	try {
    		Class.forName("com.mysql.cj.jdbc.Driver");
    		conn = DriverManager.getConnection(dburl, dbUser, dbpasswd);
    		String sql = "SELECT name, phone, companyname FROM BUSINESSCARD WHERE name LIKE ?";
    		ps = conn.prepareStatement(sql);
    		ps.setString(1, keyword+"%");
    		
    		rs = ps.executeQuery();
    		list = new ArrayList<>();
    		while(rs.next()) {
    			String name = rs.getString(1);
    			String phone = rs.getString(2);
    			String companyName = rs.getString(3);
    			list.add(new BusinessCard(name, phone, companyName));
    			System.out.println(name + " " + phone + " " + companyName);
    		}
    		
    	} catch(Exception e) {
    		e.printStackTrace();
    	} finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
    	return list;
    	
    }

    public BusinessCard addBusinessCard(BusinessCard businessCard){
    	
    	Connection conn = null;
    	PreparedStatement ps = null;
    	
    	
    	try {
    		Class.forName("com.mysql.cj.jdbc.Driver");
    		conn = DriverManager.getConnection(dburl, dbUser, dbpasswd);
    		String sql = "INSERT INTO BUSINESSCARD (name,phone,companyname) VALUES(?,?,?)";
    		ps = conn.prepareStatement(sql);
    		ps.setString(1, businessCard.getName());
    		ps.setString(2, businessCard.getPhone());
    		ps.setString(3, businessCard.getCompanyName());
    		ps.executeUpdate();
    		
    	} catch(Exception e) {
    		e.printStackTrace();
    	} finally {
			if(ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
    	return businessCard;
    }
}
