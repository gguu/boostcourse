<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>전달받은 데이터</h1>
	<ul>
		<li>first: <%=request.getParameter("first")%></li>
		<li>second: <%=request.getParameter("second")%></li>
		<li>third: <%=request.getParameter("third")%></li>
		<li>fourth: <%=request.getParameter("fourth")%></li>
		<li>fifth: <%=request.getParameter("fifth")%></li>
	</ul>
	<ul>
		<li>first: <%=request.getParameter("first")%></li>
		<li>second: <%=request.getParameter("second")%></li>
		<li>third: <%=request.getParameter("third")%></li>
		<li>fourth: <%=request.getParameter("fourth")%></li>
		<li>fifth: <%=request.getParameter("fifth")%></li>
	</ul>
	<ul>
		<li>first: <%=request.getParameter("first")%></li>
		<li>second: <%=request.getParameter("second")%></li>
		<li>third: <%=request.getParameter("third")%></li>
		<li>fourth: <%=request.getParameter("fourth")%></li>
		<li>fifth: <%=request.getParameter("fifth")%></li>
	</ul>
</body>
</html>