package kr.or.connect.jdbc.exam;

import kr.or.connect.jdbc.exam.dao.RoleDao;
import kr.or.connect.jdbc.exam.dto.Role;

public class JDBCExam2 {
	public static void main(String[] args) {
		int roleId = 500;
		String desc = "CTO";
		
		Role role = new Role(roleId, desc);
		
		RoleDao dao = new RoleDao();
		int insertCount = dao.addRole(role);
		
		System.out.println(insertCount);
		
	}
}
