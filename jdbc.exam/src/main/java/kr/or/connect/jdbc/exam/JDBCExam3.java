package kr.or.connect.jdbc.exam;

import java.util.ArrayList;
import java.util.List;

import kr.or.connect.jdbc.exam.dao.RoleDao;
import kr.or.connect.jdbc.exam.dto.Role;

public class JDBCExam3 {
	public static void main(String[] args) {
		List<Role> list = new ArrayList<>();
		RoleDao dao = new RoleDao();
		list = dao.getRoles();
		for(int i = 0 ; i < list.size() ; i++) {
			System.out.println(list.get(i).toString());
		}
	}
}
